#!/usr/bin/env bash

# Set permissions on mountpoints
for directory in \
    /home/buildstream/.cache/ \
    /home/buildstream/.cache/buildstream \
    /home/buildstream/project/.bst \
    /home/buildstream/workspaces
do
    if [ -d "${directory}" ]; then
        chown buildstream:buildstream "${directory}"
    fi
done

if [ -z ${http_proxy} ]; then
    unset http_proxy
fi

if [ -z ${https_proxy} ]; then
    unset https_proxy
fi

gosu buildstream "${@}"


#   Copyright (C) 2019 Bloomberg Finance L.P.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
